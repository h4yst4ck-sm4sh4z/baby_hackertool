#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <openssl/md5.h>
#include <getopt.h>

#define BUF_SIZE 128

void usage(char *prog)
{
  printf("-=[ Hackertool Defcon CTF ]=-\n");
  printf("%s -i [ipaddress/cidr]\n\n",prog);
  printf("\t-i\tipaddress/cidr\n");
  printf("\t-v\tverbose\n");
  printf("\t-f\tfile to write\n");
  printf("\n");
  exit(EXIT_SUCCESS);
}

unsigned long convert_ip(const char *t_addr)
{
  int octet1 = 0;
  int octet2 = 0;
  int octet3 = 0;
  int octet4 = 0;
  
  if(sscanf(t_addr, "%d.%d.%d.%d", &octet1, &octet2, &octet3, &octet4) < 1)
  {
    fprintf(stderr, "[-] Not a standard IPv4 IP address.\n");
    exit(EXIT_FAILURE);
  }
  return((octet1 << 24) | (octet2 << 16) | (octet3 << 8) | octet4);
}

unsigned long add_masking(const char *t_addr, int mask)
{
  if(mask > 32 || mask < 0) /* check for valid values */
  {
    fprintf(stderr, "[-] Not a valid subnet mask.\n");
    exit(EXIT_FAILURE);
  }
  return((int)pow(2, 32 - mask) + convert_ip(t_addr) -1);
}

char *reverse_int(unsigned long addr)
{
  static char *buffer[BUF_SIZE];
  
  snprintf((char *)buffer, BUF_SIZE, "%d.%d.%d.%d\n",
      (addr & 0xff000000) >> 24,
      (addr & 0x00ff0000) >> 16,
      (addr & 0x0000ff00) >> 8, 
      (addr & 0x000000ff));
  return((char *) buffer);
}


int enumsubnet(char *target, char *filename, int verbose)
{
  unsigned char c[MD5_DIGEST_LENGTH];
  MD5_CTX mdContext;
  FILE *fp;
  char *ip;
  char *network, *mask;
  unsigned long start = 0, end = 0, current = 0;
  int i =0;
  int progress = 0;

  if (filename)
  {
    fp = fopen(filename, "w");
    if(NULL == fp)
    {
      printf("[-] fopen() Error!!!\n");
      exit(EXIT_FAILURE);
    } else {
      printf("[+] Writing to file %s\n", filename);
    }
  }
  network = strtok(target, "/");
  mask = strtok(NULL, "/");
  
  start = convert_ip(network);
  end = add_masking(network, atoi(mask));
  
  MD5_Init (&mdContext);
  
  for(current = start; current <= end; current++)
  {
    ip = reverse_int(current);
    if (progress == 8421504)
    {
      printf(".");
      progress = 0;
    }
    if (filename)
    {
      fprintf(fp, "%s", ip); 
    }

    if (verbose)
      printf(ip);
    MD5_Update (&mdContext, ip, strlen(ip) );
    if (current == -1)
      break;
      
    progress++;
  }
  if (filename)
  {
    fclose(fp);
  }
  MD5_Final (c,&mdContext);
  printf("\nMD5: ");
  for(i = 0; i < MD5_DIGEST_LENGTH; i++) 
    printf("%02x", c[i]);
  printf("\n");
  return 0;
}


int main(int argc, char *argv[])
{
  char ch;
  int verbose = 0;
  char *target = NULL;
  char *filename = NULL;

  while ((ch = getopt(argc, argv, "i:f:hv")) != EOF)
  {
    switch(ch)
    {
      case 'i':
        target = optarg;
        break;
      case 'f':
        filename = optarg;
        break;
      case 'h':
        usage(argv[0]);
        exit(EXIT_SUCCESS);
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }
  }

  if (argc < 2)
    usage(argv[0]);

  if(!target)
  {
    printf("[-] No network specified\n");
    exit(EXIT_FAILURE);
  } else {
    printf("[+] Enumerating %s\n", target);
    enumsubnet(target, filename, verbose);
  }
  return 0;
}
